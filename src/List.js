import React from 'react';


function List(props){
    return(
        <div className='list-todo'>
            {
                props.items.map((item, index) =>
                <p key={index}>{item}</p>
                )
            }
            
        </div>
    )
}

export default List;