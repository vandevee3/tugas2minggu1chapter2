import React, { useState } from 'react';
import TodoForm from './TodoForm';
import './Todo.css'

function Todo({todos, completeTodo, removeTodo, updateTodo}){
    const [edit, setEdit] = useState({
        id : null,
        value: ''
    })

    const submitUpdate = value =>{
        updateTodo(edit.id, value)
        setEdit({
            id : null,
            value : ''
        })
    }

    if(edit.id){
        return <TodoForm edit={edit} onSubmit={submitUpdate}/>
    }

    return todos.map((todo, index) => (
        <div className='todoList-row'>
            <div className='todo-row' key={index}>
            <div key={todo.id} onClick={()=> completeTodo(todo.id)} className ='todo-text'>
                {todo.text}
            </div>
            <div className='todo-buttons'>
                <button onClick={() => setEdit({id : todo.id, value : todo.text})} className='edit-icon'>Edit</button>
                <button onClick={() => removeTodo(todo.id) } className='delete-icon' >Delete</button>
            </div>
            </div>
        </div>
    )); 
}

export default Todo;