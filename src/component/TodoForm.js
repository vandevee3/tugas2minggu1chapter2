import React, {useState, useEffect, useRef} from 'react';
import './Todo.css'

function TodoForm(props, todoState) {

    const [input, setInput] = useState(props.edit ? props.edit.value : '');

    const inputRef = useRef(null)

    useEffect(() => {
        inputRef.current.focus();
    })

    const handleSubmit = event =>{
        event.preventDefault();

        props.onSubmit({
            id : Math.floor(Math.random() *  1000),
            text : input
        });

        setInput('')
 
    };

    const countTodo = () =>{
        
    }

    const handleChange = event =>{
        setInput(event.target.value);
    }
    
 
    return( 
        <form className='todo-form' onSubmit={handleSubmit}>
            {props.edit ? ( <div>
                <div className='task-container'>
                    <input type='text' placeholder='Task' value={input} name='text' className='todo-input' onChange={handleChange} ref={inputRef}/>
                    <button className='todo-button-edit'>Update</button>
                </div>
            </div>
            ) : ( <div>
                <h1 className='todo-string'>Todo's</h1>
                <div className='task-container'>
                    <input type='text' placeholder='Task' value={input} name='text' className='todo-input' onChange={handleChange} ref={inputRef}/>
                    <button className='todo-button' >Add</button>
                </div>
            </div>   
            )}
        </form>
    );
}

export default TodoForm;