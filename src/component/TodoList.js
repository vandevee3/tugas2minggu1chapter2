import React, {useReducer, useState} from 'react';
import TodoForm from './TodoForm';
import Todo from './Todo';
import './Todo.css'

function TodoList(){
    const[todos, setTodos] = useState([]);

    const[counterTodo, setCount] = useState(0);

    const addTodo = todo =>{
        if (!todo.text || /^\s*$/.test(todo.text)){
            return;
        }

        const newTodos =[todo, ...todos];

        setTodos(newTodos);

        setCount(counterTodo  + 1);
        
        console.log(...todos);
    }

    const removeTodo = id =>{
        const removeArr = [...todos].filter(todo => todo.id !== id)

        setTodos(removeArr)
        setCount(counterTodo  - 1);
    }


    const updateTodo = (todoId, newValue) => {
        if (!newValue.text || /^\s*$/.test(newValue.text)){
            return;
        }

        setTodos(prev => prev.map(item => (item.id == todoId ? newValue : item)))
    }

    const completeTodo = id =>{
        let updatedTodos = todos.map(todo =>{
            if(todo.id === id){
                todo.isComplete =! todo.isComplete
            }
            return todo;
        })
        setTodos(updatedTodos);
    }

    return(
        <div className='container'>
            <div className='todo-container'>
                <TodoForm onSubmit={addTodo} />
                <div className='createfirst-todo'>
                    {counterTodo == 0 ? <p className='string-create'>Create Your First Todo :)</p> : <></>}
                </div>
                <Todo 
                    todos={todos}
                    completeTodo = {completeTodo}
                    removeTodo = {removeTodo}
                    updateTodo = {updateTodo}
                />
            </div>
        </div>
    )
}

export default TodoList;